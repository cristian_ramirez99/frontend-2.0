function loadEvents() {
    getUserJSON();
}
function getUserId() {
    var queryString = window.location.search;
    var posIgual = queryString.indexOf("=");
    var user_id = queryString.slice(posIgual + 1, queryString.length);

    return user_id;
}
function processUser() {
    if (this.readyState == 4 && this.status == 200) {

        var obj = JSON.parse(this.responseText);

        //i=0 no se lee porque seria la entidad User 
        for (let i = 1; obj.length > i; i++) {
            var fichaje_id = obj[i]._id.$oid;

            console.log(fichaje_id);
            getFichajeJSON(fichaje_id);
        }
    }
}
function processFichaje() {
    if (this.readyState == 4 && this.status == 200) {

        e = document.getElementById("fichajes");

        var obj = JSON.parse(this.responseText);
        var fecha = obj.date;
        var tipo = obj.tipo;
        var altitud = obj.pos.altitud;
        var latitud = obj.pos.latitud;

        //Slice fecha 
        var dia = fecha.slice(0, 2);
        var mes = fecha.slice(3, 5);
        var año = fecha.slice(6, 10);
        var hora = fecha.slice(11, 13);
        var minutos = fecha.slice(14, 16);

        if (minutos < 10) {
            minutos = 0 + minutos;
        }

        e.innerHTML += "<tr>" +
            "<td>" + tipo + "</td>" +
            "<td>" + dia + "</td>" +
            "<td>" + mes + "</td>" +
            "<td>" + año + "</td>" +
            "<td>" + hora + ":" + minutos + "</td>" +
            "<td>[" + altitud + "," + latitud + "]</td>" +
            "</tr>";
    }
}
function getUserJSON() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = processUser;
    xhr.open("GET", "http://localhost:5000/user/" + getUserId(), true);
    xhr.send();
}
function getFichajeJSON(fichaje_id) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = processFichaje;
    xhr.open("GET", "http://localhost:5000/user/" + getUserId() + "/fichaje/" + fichaje_id, true);
    xhr.send();
}