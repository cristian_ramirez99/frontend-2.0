FROM ubuntu:20.04
WORKDIR /appli
COPY . /appli
RUN apt-get update -y
RUN apt-get install python3-dev -y
RUN apt install python3-pip -y
RUN pip3 install Flask
RUN pip3 
ENTRYPOINT ["python3"]
CMD ["app.py"]
# para ejecutar el flask necesitamos hacer en la carpeta del archivo .py
# export FLASK_APP=nombre.py
# flask run
